import { User } from '../../models/index.js';

const isUserLoggedIn = async (req, res, next) => {
  if (!req.session.userId) {
    res.redirect('/login');
    return;
  }

  let user = await User.findById(req.session.userId);
  if (!user) {
    res.redirect('/login');
    return;
  }
  next();
}

const restrictAccessIfUserNotLoggedIn = async () => {
  
}

export default isUserLoggedIn;