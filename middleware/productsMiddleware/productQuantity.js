import mongoose from 'mongoose';
import { Product } from '../../models/index.js';

const isProductGreaterThanZero = async (req, res, next) => {
  let productToUpdate = await Product.findOne({SKU: req.body.SKU});

  if (!productToUpdate) {
    res.status(500);
    res.render('products/removeStocks', { err: `Erreur: Impossible de trouver le produit avec le SKU [ ${req.body.SKU} ].`});
    return;
  }

  if (productToUpdate.Quantity - req.body.Quantity < 0) {
    res.status(500);
    res.render('products/removeStocks', {err: 'La quantité du produit ne peut pas être inférieur à 0.'});
    return;
  }
  next();
}

export default isProductGreaterThanZero;