import express from 'express';
import ejs from 'ejs';
import mongoose from 'mongoose';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import { isUserLoggedIn, isProductGreaterThanZero } from './middleware/index.js';
import { 
  HomeController,
  ProductController,
  UserController } from './controllers/index.js';
import connectflash from 'connect-flash';


const app = new express();
const PORT = process.env.PORT ? process.env.PORT : 4000;
const MONGODB_HOSTNAME = process.env.MONGODB_HOSTNAME ? process.env.MONGODB_HOSTNAME : 'localhost';
const MONGODB_PORT = process.env.MONGODB_PORT ? process.env.MONGODB_PORT : 27017;
let productController = new ProductController();
let userController = new UserController();

app.set("view engine", "ejs");

app.use(express.static("public"));

mongoose.set("strictQuery", false);
mongoose.connect(
  `mongodb://${MONGODB_HOSTNAME}:${MONGODB_PORT}/${process.env.MONGODB_DATABASE}`,
).then(console.log("DB connected."));

app.use(
  expressSession({
    secret: process.env.SESSIONKEY ? process.env.SESSIONKEY : "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3",
    resave: true,
    saveUninitialized: true,
  })
);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(fileUpload());

app.use(connectflash());

global.isLoggedIn = null;
app.use("*", (req, res, next) => { 
  isLoggedIn = req.session.userId; 
  next();
});

app.use('/products', isUserLoggedIn);

/**
 * simple check-up to see if server is online.
 */
app.get('/ping', (req,res) => {
  res.end('pong');
})

app.get('/', HomeController );

app.get('/login', userController.viewLogin);

app.get('/products', productController.viewIndex);

app.get('/products/create', productController.viewCreate)

app.get('/products/addStocks', productController.viewAddStock)

app.get('/products/removeStocks', productController.viewRemoveStock)

app.get('/products/delete', productController.viewSoftDelete);

app.post('/login', userController.login);

app.post('/logout', () => {}); // TODO: logout

app.post('/products/delete', productController.softDelete);

app.post('/product/create', productController.create);

app.post('/products/addStocks', productController.addStocks);

app.post('/products/removeStocks', isProductGreaterThanZero, productController.removeStocks);

app.post('/products/search', productController.searchProducts)

app.use((req, res) => res.end('error404')); // redirect to "404" if route is not found.

// app.use((req, res) => res.render('error404')); // redirect to "404" if route is not found.
  app.listen( PORT, () => {
    console.log(`App listening on port : ${PORT} \nnavigate to: http://localhost:${PORT}`);
  });