import mongoose from 'mongoose';
import { User } from '../models/index.js';
import bcrypt from 'bcrypt';

const MONGODB_HOSTNAME = process.env.MONGODB_HOSTNAME ? process.env.MONGODB_HOSTNAME : 'localhost';
const MONGODB_PORT = process.env.MONGODB_PORT ? process.env.MONGODB_PORT : 27017;
const MONGODB_DATABASE= process.env.MONGODB_DATABASE ? MONGODB_DATABASE : 'MegaBoutique';
const username = "admin";
const email = "admin@hotmail.com";
const password = "password";

mongoose.set("strictQuery", false);
await mongoose.connect(
  `mongodb://${MONGODB_HOSTNAME}:${MONGODB_PORT}/${MONGODB_DATABASE}`,
).then( async () => {
  console.log("DB connected.");

  const hashedPassword = await bcrypt.hash(password, 10);
  console.log(`creating first user with password ${password}`);
  await User.create({
    Email: email,
    Username: username,
    Password: hashedPassword
  })
  console.log("User successfully created.");
});