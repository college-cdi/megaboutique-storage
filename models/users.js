import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  "Email": {
    type: String, 
    required: true, 
    index: true, 
    unique: true
  },
  "Password": {
    type: String, 
    required: true
  },
  "Username": {
    type: String,
    required: true
  },
  "PasswordExpired": {
    type: Boolean,
    default: true
  },
  "DateCreated": {
    type: Date,
    default: Date.now
  },
  "DateUpdated": {
    type: Date,
    default: null
  }
}) 

// const RoleSchema = new Schema({
//   "Name": {
//     type: String,
//     required: true
//   }
// })

// const UserRoleSchema = new Schema({
//   "User": { type: mongoose.Types.ObjectId, ref: "Users" },
//   "Role": { type: mongoose.Types.ObjectId, ref:"Roles" }
// })

// const PermissionSchema = new Schema({
//   "Name": {
//     type: String,
//     required: true
//   }
// })

// const RolePermissionSchema = new Schema({
//   "Role": { type: mongoose.Types.ObjectId, ref: "Roles"},
//   "Permission": { type: mongoose.Types.ObjectId, ref: "Permission" }
// })

const User = mongoose.model('Users', UserSchema);
// const Role = mongoose.model('Roles', RoleSchema);
// const Permission = mongoose.model("Permissions", PermissionSchema);
// const RolePermission = mongoose.model('RolePermissions', RolePermissionSchema);
// const UserRole = mongoose.model('UserRoles', UserRoleSchema);

export default User;
// export default Role;
// export default Permission;
// export default RolePermission;
// export default UserRole;