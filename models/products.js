import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
  "SKU": {
    type: String, 
    required: true, 
    index: true, 
    unique: true
  },
  "Name": {
    type: String, 
    required: true
  },
  "Price": {
    type: Number,
    required: true
  },
  "Image": {
    type: String,
    required: true
  },
  "Quantity" : {
    type: Number,
    default: 0,
    validate : {
      validator : Number.isInteger,
      message   : '{VALUE} is not an integer value'
    }
  },
  "Status": {
    type: String
  },
  "DateCreated": {
    type: Date,
    default: Date.now
  },
  "DateUpdated": {
    type: Date,
    default: null
  }
}) 

const Product = mongoose.model('Products', ProductSchema);
export default Product;