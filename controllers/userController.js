import { User } from "../models/index.js";
import bcrypt from "bcrypt";

export default class UserController {

  viewLogin(req, res) {
    res.render('login');
  }

  async login(req, res) {
    console.log("Loggin in");
    const { username, password } = req.body;
    let user = await User.findOne({Username: username});

    if (!user) {
      res.redirect('/login');
      return;
    }

    bcrypt.compare(password, user.Password, (error, same) => {
      if (same) {
        // TODO: Save login in cookies.
        req.session.userId = user._id;
        res.redirect("/");
      } else {
        res.redirect("/login");
      }
    });
  }

}