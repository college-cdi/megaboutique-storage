import { Product } from '../models/index.js';
import path from 'path';
import { fileURLToPath } from 'url';

export default class ProductController {


  async viewIndex(req, res) {
    let products = await Product.find({ Status: { $ne: 'deleted'}});
    res.render("products/index", {products: products, message: { success: req.flash('success'), error: req.flash('error')} });
  }


  viewSoftDelete(req, res) {
    res.render("products/delete", { err: req.flash('error') });
  }

  
  viewAddStock(req, res) {
    res.render("products/addStocks", { err: req.flash('error') });
  }


  viewRemoveStock(req, res) {
    res.render("products/removeStocks", { err: req.flash('error') });
  }


  viewCreate(req, res) {
    res.render('products/create', { err: req.flash('error') });
  }


  async create(req, res) {
    let image = req.files.Image;
    const __filename = fileURLToPath(import.meta.url);
    const __dirname = path.dirname(__filename);
    const imagePath = path.resolve(__dirname, '../public/assets/img/', image.name);
    // look if file exists, return err if exists.

    await image.mv((imagePath), async (error) => {
      if (error) {
        req.flash('error', error)
        res.status(500);
        res.render('products/create', { err: req.flash('error') });
        return;
      }
    })
    let newProduct = await Product.create({...req.body, Image: '/assets/img/' + image.name })
    .catch((err) => {
      if (err.message.includes('E11000')) {
        req.flash('error', `La création du produit à échoué car un produit avec le SKU ${req.body.SKU} existe déjà.`)
      } else {
        req.flash('error', 'Une erreur est survenue lors de la création du produit... Si le problème persiste, contactez votre administrateur.');
      }
      // delete file? 
      res.status(500);
      res.render('products/create', { err: req.flash('error') });
      return;
    })

    if (newProduct) {
      req.flash('success', 'Produit créer avec succès!')
      res.redirect('/');
    }
  }

  
  async addStocks(req, res) {
    let productToUpdate = await Product.updateOne({ SKU: req.body.SKU }, { $inc: { Quantity: req.body.Quantity }, DateUpdated: Date.now() }).catch((err) => {
      console.log(err);
    });

    if (!productToUpdate) {
      req.flash('error', `Erreur: Impossible de trouver le produit avec le SKU [ ${req.body.SKU} ].` )
      res.status(500);
      res.render('products/addStocks', { err: req.flash('error')});
      return;
    }
    req.flash('success', 'Stocks ajouté au produit avec succès!');
      res.redirect("/");
  }
  

  async removeStocks(req, res) {
    let productToUpdate = await Product.findOne({SKU: req.body.SKU});
    
    await Product.updateOne(productToUpdate, {$inc: { Quantity: req.body.Quantity * - 1}, DateUpdated: Date.now() }).catch((err) => {
      res.status(500);
      res.render('products/removeStocks', {err: err});
      return;
    })
    req.flash('success', 'Stocks envoyé avec succès!')
    res.redirect('/');
  }


  async softDelete(req, res) {
    let product = await Product.findOneAndUpdate({SKU: req.body.SKU}, { Status: 'deleted', DateUpdated: Date.now() });
    if (product) { 
      req.flash('success', 'Produit supprimé avec succès!');
      console.log(product);
    } else {
      req.flash('error', 'Le produit n\'a pas pu être supprimé.');
      console.log('Product not found');
    }
    res.redirect('/');
  }

  async searchProducts(req, res) {
    let products = await Product.find({ Name : new RegExp(req.body.Search, 'i'), Status: { $ne: 'deleted'}});
    if (!products) {
      req.flash('error', 'Aucun produit n\'a été trouvé.');
    }
    res.render("products/index", {products: products, message: req.flash('success'), error: req.flash('error')});
  }

}