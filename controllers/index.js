export { default as HomeController } from './pages/home.js';

export { default as UserController } from './userController.js';
export { default as ProductController } from './productController.js';