import { Product } from '../../models/index.js';


const HomeController = async (req, res) => {
    let products = await Product.find({ Status: { $ne: 'deleted'}});
    res.render('index', {totalItems: products.length, message: { success: req.flash('success'), err: req.flash('error')}});
}

export default HomeController;